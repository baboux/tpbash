#! /bin/bash
for ligne in $(tree -fi | grep jpg$)
do
    convert $ligne -geometry 200x260^ -gravity center -crop 200x260+0+0 $ligne
done