#! /bin/bash
mkdir -p OutPuts
tree -fi | grep -ie jpg$ -ie png$ -ie tif$ -ie bmp$ > fichier_temp.txt
for (( var=0; var <= $(($(wc -l <fichier_temp.txt)-1)); var++ ))
do
    aaa=$(sed -n "$(($var+1)) p" ./fichier_temp.txt | rev | cut -f 1 -d '/' | rev)
    nom='fichier_'
    nom+=$(echo ${aaa::3})
    nom+='_num_'
    nom+=$var
    nom+='.jpg'
    convert $(sed -n "$(($var+1)) p" ./fichier_temp.txt) ~/rep_etude/OutPuts/$(echo $nom)
done
rm fichier_temp.txt